package TO;

public class datosXsdTO {
	
	/**
	 * indica si es un complex o element.
	 */
	private String nivel;
	
	private int nivelJerarquia;
	
	private String name;
	
	private String nillable;
	
	private String maxOccurs;
	
	private String minOccurs;
	
	private String type;

	private boolean esComplextType;
	
	private String restriccionMax;
	private String restriccionMin;

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public int getNivelJerarquia() {
		return nivelJerarquia;
	}

	public void setNivelJerarquia(int nivelJerarquia) {
		this.nivelJerarquia = nivelJerarquia;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNillable() {
		return nillable;
	}

	public void setNillable(String nillable) {
		this.nillable = nillable;
	}

	public String getMaxOccurs() {
		return maxOccurs;
	}

	public void setMaxOccurs(String maxOccurs) {
		this.maxOccurs = maxOccurs;
	}

	public String getMinOccurs() {
		return minOccurs;
	}

	public void setMinOccurs(String minOccurs) {
		this.minOccurs = minOccurs;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isEsComplextType() {
		return esComplextType;
	}

	public void setEsComplextType(boolean esComplextType) {
		this.esComplextType = esComplextType;
	}

	public String getRestriccionMax() {
		return restriccionMax;
	}

	public void setRestriccionMax(String restriccionMax) {
		this.restriccionMax = restriccionMax;
	}

	public String getRestriccionMin() {
		return restriccionMin;
	}

	public void setRestriccionMin(String restriccionMin) {
		this.restriccionMin = restriccionMin;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("datosXsdTO [nivel=");
		builder.append(nivel);
		builder.append(", nivelJerarquia=");
		builder.append(nivelJerarquia);
		builder.append(", name=");
		builder.append(name);
		builder.append(", nillable=");
		builder.append(nillable);
		builder.append(", maxOccurs=");
		builder.append(maxOccurs);
		builder.append(", minOccurs=");
		builder.append(minOccurs);
		builder.append(", type=");
		builder.append(type);
		builder.append(", esComplextType=");
		builder.append(esComplextType);
		builder.append(", restriccionMax=");
		builder.append(restriccionMax);
		builder.append(", restriccionMin=");
		builder.append(restriccionMin);
		builder.append("]");
		return builder.toString();
	}
	
}
