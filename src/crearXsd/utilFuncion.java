package crearXsd;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Map.Entry;

public class utilFuncion {

	private static Hashtable tbl   = new Hashtable();
	/**
     * Cambia el contenido del mensaje por la cadena valor correspondiente
     * @param string contiene el mensaje.
     * @param map los datos a incluir en el mensaje, cade valor.
     * @return string con el mensaje a mostrar.
     */
	public static String formatString(String string, Map map){
        Iterator iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry entry = (Entry) iterator.next();
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            string = string.replaceAll("&"+key+"&", value);
        }
        return string;
    }
    
    /**
     * Lectura de tabla
     * @param file direccion de archivo.
     * @param clave dato a buscar.
     * @param cod valor a rescatar.
     * @return valor rescatado del archivo. 
     */
    public static String getValor(String file, String clave, String cod) {

        if (tbl.get(file) == null){
            Hashtable ht = loadTable(file);
            if (tbl.get(file) == null)
                tbl.put(file, ht);
        }
        Hashtable a = (Hashtable)tbl.get(file);
        Hashtable b = (Hashtable)a.get(clave.toUpperCase());
        String ret = null;
        if (b != null) {
            ret = (String)b.get(cod.toUpperCase());
        }
        return ret;
    }
    
    /**
     * Carga información de una tabla de parámetros para dejarla en memoria. 
     * @param archivo nombre de la tabla de parametros a cargar.
     * @return
     */
    private synchronized static Hashtable loadTable(String archivo) {        
        String arch = archivo; 
        String record = null;
        String tmp = null;
        String mainkey = null;
        String seckey = null;
        String secValue = null;
        Hashtable tabla = new Hashtable();
        Hashtable tblreg = null;
        int pos = 0;

        try {
            FileReader fr = new FileReader(arch);
            BufferedReader br = new BufferedReader(fr);
            record = new String();

            while ((record = br.readLine()) != null) {
                if (!record.trim().equals("") & !record.trim().startsWith("#")){
                    tblreg = new Hashtable();
                    StringTokenizer stTok = new StringTokenizer(record);
                    tmp = stTok.nextToken(";");
                    mainkey = tmp == null ? "" : tmp.trim().toUpperCase();
                    while (stTok.hasMoreTokens()) {
                        try{
                            tmp = stTok.nextToken("=;");
                            seckey = tmp == null ? "" : tmp.trim().toUpperCase();
                            tmp = stTok.nextToken(";");
                            secValue = tmp == null ? "" : tmp.substring(1).trim();
                            tblreg.put(seckey, secValue);
                         }
                        catch(NoSuchElementException e){
           
                            secValue = "";
                         }
                        catch(StringIndexOutOfBoundsException e){
                            secValue = "";
                         }
                    }
                    tblreg.put("_Posicion", String.valueOf(pos));
                    pos++;
                    tabla.put(mainkey, tblreg);
                }
            }            
            

        } 
        catch (IOException ioe) {
        	System.out.println("IOException : " + ioe);
            
        }
        return tabla;
    }
}
