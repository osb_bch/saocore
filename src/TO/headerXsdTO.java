package TO;

public class headerXsdTO {
	
	private String targetNamespace;
	
	private String elementFormDefault;
	
	private String xsd;
	
	private String xmlns;
	
	private String tns;
	
	private String comun;
	
	private String location;

	public String getTargetNamespace() {
		return targetNamespace;
	}

	public void setTargetNamespace(String targetNamespace) {
		this.targetNamespace = targetNamespace;
	}

	public String getElementFormDefault() {
		return elementFormDefault;
	}

	public void setElementFormDefault(String elementFormDefault) {
		this.elementFormDefault = elementFormDefault;
	}

	public String getXsd() {
		return xsd;
	}

	public void setXsd(String xsd) {
		this.xsd = xsd;
	}

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}

	public String getTns() {
		return tns;
	}

	public void setTns(String tns) {
		this.tns = tns;
	}

	public String getComun() {
		return comun;
	}

	public void setComun(String comun) {
		this.comun = comun;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("headerXsdTO [targetNamespace=");
		builder.append(targetNamespace);
		builder.append(", elementFormDefault=");
		builder.append(elementFormDefault);
		builder.append(", xsd=");
		builder.append(xsd);
		builder.append(", xmlns=");
		builder.append(xmlns);
		builder.append(", tns=");
		builder.append(tns);
		builder.append(", comun=");
		builder.append(comun);
		builder.append(", location=");
		builder.append(location);
		builder.append("]");
		return builder.toString();
	}
}
