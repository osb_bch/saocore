package crearXsd;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

import TO.datosXsdTO;
import TO.headerXsdTO;

/**
 * Servicio que lee un archivo que contiene la informacion para generar un xsd, luego de leer crea el archivo xsd correspondiente.
 * @author sentrauser3233 <<Gonzalo Morales>>
 * @version 1.0
 */
public class Main {

	/**
	 * Row que se omitiran del excel.
	 */
	private static int OMITIR_ROW = 4;
	
	/**
	 * Valor a omitir del excel o indica que es tipo complexType;
	 */
	private static String CADENA_A_OMITIR ="---";
	
	/**
	 * contiene el contenido del excel en forma de lista.
	 */
	private static List<List<datosXsdTO>> LISTA_ESQUEMA = new ArrayList<List<datosXsdTO>>();
	
	/**
	 * indica cantidad de niveles que tiene el objeto lista.
	 */
	private static int capasIngresadas = -1;
	
	/**
	 * Marca que separa el nivel con el subnivel.
	 */
	private static String SEPARADOR_NIVEL =".";
	
	/**
	 * Nombre archivo de entrada.
	 */
	private static String FILE_NAME = "entradaXsd.xls";
	
	
	/**
	 * contiene la estructura del xsd.
	 */
	private static String PROPERTIE = "./tabla/estructuraXsd.parametros";
	
	/**
	 * descripcion del elemento en la tabla.
	 */
	private static String EST = "est";
	
	/**
	 * Estructura del header del xsd.
	 */
	private static String HEADER_XSD = "HEADER_XSD";
	
	/**
	 * Estructura del header del xsd con import de datos comunes.
	 */
	private static String HEADER_XSD_COMUN = "HEADER_XSD_COMUN";
	
	/**
	 * Estructura del para importar datos en el header.
	 */
	private static String HEADER_XSD_IMPORT = "HEADER_XSD_IMPORT";
	
	/**
	 * Contiene la estructura del element fuera de un complext.
	 */
	private static String ELEMENTO_XSD = "ELEMENTO_XSD";
	
	/**
	 * Inicio de elemento tipo ComplexType.
	 */
	private static String ELEMENTO_COMPLEX_TYPE_INI = "ELEMENTO_COMPLEX_TYPE_INI";

	/**
	 * Element dentro de un complext.
	 */
	
	private static String ELEMENTO_XSD_COMPLEX = "ELEMENTO_XSD_COMPLEX";
	/**
	 * Cierra el complexType.
	 */
	private static String ELEMENTO_COMPLEX_TYPE_FIN = "ELEMENTO_COMPLEX_TYPE_FIN";

	/**
	 * Nombre archivo de salida.
	 */
	private static String XSD_SALIDA;

	/**
	 * Inicio de elemento con restriccion.
	 */
	private static String ELEMENTO_RESTRICTION = "ELEMENTO_RESTRICTION";

	/**
	 * Estructura restriccion para string.
	 */
	private static String ELEMENTO_RESTRICTION_STRING = "ELEMENTO_RESTRICTION_STRING";

	/**
	 * Estructura restriccion para int o integer.
	 */
	private static String ELEMENTO_RESTRICTION_INT = "ELEMENTO_RESTRICTION_INT";

	/**
	 * Estructura restriccion para decimal.
	 */
	private static String ELEMENTO_RESTRICTION_DECI = "ELEMENTO_RESTRICTION_DECI";
	
	public static void main(String[] args) throws Exception {
		generarXsd(FILE_NAME);
		 
	}
	
	
	public static String generarXsd(String ruta) throws Exception{
		/**
		 * nombre del archivo de entrada.
		 * NO CAMBIAR EXTENCION Y ESTRUCTURA DEL ARCHIVO.
		 */
		
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(ruta);
			HSSFWorkbook workbook = new HSSFWorkbook(fis);
			HSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<?> rows = sheet.rowIterator();
			int numColl = 0;
			HSSFRow row = null;
			

			headerXsdTO hederXsd = new headerXsdTO();
			row = (HSSFRow) rows.next();
			for (int i = 0; i < OMITIR_ROW; i++) {
				Iterator<?> cells = row.cellIterator();
				cells.next();
				HSSFCell cell = (HSSFCell) cells.next();
				
				switch (i) {
				case 0:
					XSD_SALIDA = cell.toString().trim();
					File fichero = new File("./archivosOut/"+XSD_SALIDA);
					if (fichero.exists()) {
						BufferedWriter bw = new BufferedWriter(new FileWriter("./archivosOut/"+XSD_SALIDA));
						bw.write("");
						bw.close();
					}
					break;
				case 1:
					hederXsd.setTargetNamespace(cell.toString().trim());
					hederXsd.setTns(cell.toString().trim());
					System.out.println("["+i+"]["+hederXsd.toString()+"]");
					break;
				case 2:
					if(!cell.toString().trim().equals("")){
						hederXsd.setComun(cell.toString().trim());
						System.out.println("["+i+"]["+hederXsd.toString()+"]");
					}
					break;
				case 3:
					if(!cell.toString().trim().equals("")){
						hederXsd.setLocation(cell.toString().trim());
						System.out.println("["+i+"]["+hederXsd.toString()+"]");
					}
					break;
				default:
					break;	
				}
				row = (HSSFRow) rows.next();
				
			}
			constructorHeaderXsd(hederXsd);
			
			datosXsdTO contenidoXsd = null;
			while (rows.hasNext()) {
				row = (HSSFRow) rows.next();
				//despues de OMITIR_ROW viene el contenido del xsd.

				Iterator<Cell> cells = row.cellIterator();
				Iterator<Cell> datoControlDeVacio = row.cellIterator();
				contenidoXsd = new datosXsdTO();
				
				HSSFCell test = (HSSFCell) datoControlDeVacio.next();
				if(test.toString().trim().equals("")){
					continue;
				}
				while (cells.hasNext()) {
					HSSFCell cell = (HSSFCell) cells.next();
					
					switch (numColl) {
						case 0:   
							// NIVEL
							System.out.println("contenido: ["+ cell.toString()+"]");
								contenidoXsd.setNivel(String.valueOf(cell.toString().trim()));
							
							String identificadorNivel = contenidoXsd.getNivel();
							int nivelJerarquia=0;
							while (identificadorNivel.indexOf(SEPARADOR_NIVEL ) > -1) {
								identificadorNivel = identificadorNivel.substring(identificadorNivel.indexOf(
							        SEPARADOR_NIVEL)+SEPARADOR_NIVEL.length(),identificadorNivel.length());
							      nivelJerarquia++; 
							}
							contenidoXsd.setNivelJerarquia(nivelJerarquia);
							System.out.println("Agregear NIVEL: ["+ contenidoXsd.getNivel()+"]");
							break;
						case 1:
							// name
							contenidoXsd.setName(cell.toString().trim());
							
							//System.out.println("Agregear nombre: [" + contenidoXsd.getName()+"]");
							break;
						case 2:
							// nillable
							if(cell.toString().trim().toLowerCase().equals("true")){
								contenidoXsd.setNillable("true");
							}
							else{
								contenidoXsd.setNillable("false");
							}
							break;
						case 3:
							// minOccurs
							if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
								contenidoXsd.setMinOccurs(String.valueOf((int)cell.getNumericCellValue()));
							}
							else{
								contenidoXsd.setMinOccurs("1");
							}
							//System.out.println("Agregear minOccurs: [" + (int)cell.getNumericCellValue()+"]");
							break;
						case 4:
							// maxOccurs
							if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
								contenidoXsd.setMaxOccurs(String.valueOf((int)cell.getNumericCellValue()));
							}
							else{
								if(cell.toString().toUpperCase().equals("N") || cell.toString().toLowerCase().equals("unbounded")){
									contenidoXsd.setMaxOccurs("unbounded");
								}
								else{
									contenidoXsd.setMaxOccurs("1");
								}
							}
							//System.out.println("Agregear maxOccurs: [" + (int)cell.getNumericCellValue()+"]");
							break;
						case 5:
							// simpleType
							
							if( CADENA_A_OMITIR.equals(cell.toString().trim()) || cell.toString().trim().equals("") ){
								contenidoXsd.setType(contenidoXsd.getName()+"Type");
								contenidoXsd.setEsComplextType(true);
							}
							else{
								String type = "";
								if(cell.toString().trim().indexOf("(") >-1){
									type = cell.toString().trim().substring(0, cell.toString().trim().indexOf("("));
									
									if(type.equals("decimal")){
										String restriccionMax = cell.toString().trim().substring
												(cell.toString().trim().indexOf("(")+1,cell.toString().trim().indexOf(","));
										String restriccionMin = cell.toString().trim().substring
												(cell.toString().trim().indexOf(",")+1,cell.toString().trim().indexOf(")"));
										System.out.println("restricciones "+restriccionMax +" "+ restriccionMin);
										contenidoXsd.setRestriccionMax(restriccionMax);
										contenidoXsd.setRestriccionMin(restriccionMin);
									}
									else{
										String restriccionMax = cell.toString().trim().substring
												(cell.toString().trim().indexOf("(")+1,cell.toString().trim().indexOf(")"));
										contenidoXsd.setRestriccionMax(restriccionMax);
									}
									
									
								}
								else{
									type = cell.toString().trim();
								}
								
								if(type.trim().indexOf("comun") >-1){
									contenidoXsd.setType(type);
								}
								else{
									contenidoXsd.setType("xsd:"+type.trim());
								}
								
								contenidoXsd.setEsComplextType(false);
							}
							//System.out.println("Agregear simpleType: [" + contenidoXsd.getType()+"]["+SIN_TYPE+"]");
							
							break;
						default:
							break;
					}
					numColl++;
				}
				//se tiene TO en contenidoXsd, este contiene la informacion de la fila n
				
				List<datosXsdTO> nodoLista = new ArrayList<datosXsdTO>();
				if(capasIngresadas < contenidoXsd.getNivelJerarquia()){
					nodoLista.add(contenidoXsd);
					LISTA_ESQUEMA.add(nodoLista);
					
				}

				if (capasIngresadas >= contenidoXsd.getNivelJerarquia()){
					// agregar info a capa ya existente
					//System.out.println(LISTA_ESQUEMA.get(contenidoXsd.getNivelJerarquia()));
					ArrayList<datosXsdTO> listainsertada = (ArrayList<datosXsdTO>) LISTA_ESQUEMA.get(contenidoXsd.getNivelJerarquia());
					listainsertada.add(contenidoXsd);
				}
				
				numColl = 0;
				capasIngresadas = LISTA_ESQUEMA.size()-1;
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error [main] ::"+e);
		} 
		finally {
			if (fis != null) {
				fis.close();
			}
		}
		//System.out.println("capas Ingresadas: "+capasIngresadas);
		System.out.println(LISTA_ESQUEMA.toString());
		constructorXsd(LISTA_ESQUEMA);
		
		return XSD_SALIDA;
	}

	
	/**
	 * Se encarga de crear la cabezara del xsd
	 * @param headerXsd TO con el contenido del header.
	 */
	private static void constructorHeaderXsd(headerXsdTO headerXsd){
		try {
			System.out.println("[constructorHeaderXsd] ::"+headerXsd.toString());
			boolean ocupaComun = false;
			Map<String, String> paramheader = new HashMap<String, String>();
			
			if(headerXsd.getTargetNamespace() !=null){
				paramheader.put("targetNamespace", headerXsd.getTargetNamespace());
			}
			if(headerXsd.getElementFormDefault() !=null){
				paramheader.put("elementFormDefault", headerXsd.getElementFormDefault());
			}
			if(headerXsd.getXsd() !=null){
				paramheader.put("xsd", headerXsd.getXsd());
			}
			if(headerXsd.getXmlns() !=null){
				paramheader.put("xmlns", headerXsd.getXmlns());
			}
			if(headerXsd.getTns() !=null){
				paramheader.put("tns", headerXsd.getTns());
			}
			if(headerXsd.getComun() !=null){
				paramheader.put("comun", headerXsd.getComun());
				paramheader.put("location", headerXsd.getLocation());
				ocupaComun = true;
			}
			
			if(ocupaComun){
				String esqueleto= utilFuncion.getValor(PROPERTIE, HEADER_XSD_COMUN , EST);
				String salida= utilFuncion.formatString(esqueleto, paramheader);
				System.out.println("Salida header con Comun "+salida);
				escrituraXsd(salida,false);
				
				esqueleto = utilFuncion.getValor(PROPERTIE, HEADER_XSD_IMPORT , EST);
				salida= utilFuncion.formatString(esqueleto, paramheader);
				System.out.println("Salida import de comun "+salida);
				escrituraXsd(salida,false);
			}
			else{
				String esqueleto= utilFuncion.getValor(PROPERTIE, HEADER_XSD , EST);
				String salida= utilFuncion.formatString(esqueleto, paramheader);
				System.out.println(salida);
				escrituraXsd(salida,false);
			}
			
			
		} 
		catch (Exception e) {
			System.out.println("Exception [constructorHeaderXsd] ::"+e);
		}
	}

	/**
	 * crea la estructura del xsd.
	 * @param contenidoXsd informacion del contenido del xsd.
	 */
	private static void constructorXsd(List<List<datosXsdTO>> contenidoXsd){
		//System.out.println("constructorXsd ["+contenidoXsd.size()+"]"+contenidoXsd);
		
		try {
			boolean complexTypeAbierto= false;
			String nivelComplexActual="";
			String typeComplexActual="";
			String esqueleto="";
			Queue<String> listaComplex = new LinkedList<String>();
			
			for (int i = 0; i <= capasIngresadas; i++) {
				//System.out.println(i);
				ArrayList<datosXsdTO> capaXsd = (ArrayList<datosXsdTO>) LISTA_ESQUEMA.get(i);
				
				System.out.println("cant item: "+capaXsd.size());
				
				for (int j = 0; j < capaXsd.size(); j++) {
					datosXsdTO elementXsd = (datosXsdTO) capaXsd.get(j);
					System.out.println(elementXsd);
					
					Map<String, String> paramsDetRea = new HashMap<String, String>();
		            paramsDetRea.put("name", elementXsd.getName());
		            paramsDetRea.put("type", elementXsd.getType());
		            paramsDetRea.put("maxOccurs", elementXsd.getMaxOccurs());
		            paramsDetRea.put("minOccurs", elementXsd.getMinOccurs());
		            paramsDetRea.put("nillable", elementXsd.getNillable());
		            
		            if(elementXsd.isEsComplextType()){
						listaComplex.add(elementXsd.getNivel()+";"+elementXsd.getType());
					}
		            
		            if(elementXsd.getNivelJerarquia() == 0){
		            	esqueleto= utilFuncion.getValor(PROPERTIE, ELEMENTO_XSD , EST);
		            	if(elementXsd.isEsComplextType()){
		            		paramsDetRea.put("type", "tns:"+elementXsd.getType());
		            	}
						String salida= utilFuncion.formatString(esqueleto, paramsDetRea);
						System.out.println(salida);
						escrituraXsd(salida,false);
						
		            }
		            else{
		            	if(nivelComplexActual.equals(elementXsd.getNivel().substring(0, elementXsd.getNivel().lastIndexOf(SEPARADOR_NIVEL)))){
		            		if(complexTypeAbierto){
		            			//si esta abierto continuar llenandolo
		            			if(elementXsd.getRestriccionMax()!=null){
		            				System.out.println("restriccion:: "+elementXsd.getRestriccionMax());
		            				paramsDetRea.put("restriccionMax", elementXsd.getRestriccionMax());
		            				esqueleto= utilFuncion.getValor(PROPERTIE, ELEMENTO_RESTRICTION , EST);
		            				String salida= utilFuncion.formatString(esqueleto, paramsDetRea);
									System.out.println(salida);
									escrituraXsd(salida,false);
									
									System.out.println("DATO:: "+elementXsd.toString());
									if(elementXsd.getType().equals("xsd:string")){
										esqueleto= utilFuncion.getValor(PROPERTIE, ELEMENTO_RESTRICTION_STRING , EST);
									}
									else{
										if(elementXsd.getType().equals("xsd:int") || elementXsd.getType().equals("xsd:integer")){
											esqueleto= utilFuncion.getValor(PROPERTIE, ELEMENTO_RESTRICTION_INT  , EST);
										}
										else{
											if(elementXsd.getType().equals("xsd:decimal")){
												esqueleto= utilFuncion.getValor(PROPERTIE, ELEMENTO_RESTRICTION_DECI   , EST);
												paramsDetRea.put("restriccionMin", elementXsd.getRestriccionMin());
											}
											else{
												throw new Exception ("Error [constructorXsd] No se encuentra esqueleto en parametros.");
											}
										}
									}
									salida= utilFuncion.formatString(esqueleto, paramsDetRea);
									System.out.println(salida);
									escrituraXsd(salida,false);
										
		            			}
		            			else{
			            			esqueleto= utilFuncion.getValor(PROPERTIE, ELEMENTO_XSD_COMPLEX , EST);
			            			if(elementXsd.isEsComplextType()){
					            		paramsDetRea.put("type", "tns:"+elementXsd.getType());
					            	}
									String salida= utilFuncion.formatString(esqueleto, paramsDetRea);
									System.out.println(salida);
									escrituraXsd(salida,false);
		            			}
		            		}
		            		else{
		            			esqueleto = utilFuncion.getValor(PROPERTIE, ELEMENTO_COMPLEX_TYPE_INI, EST);
	        				
		        				Map<String, String> param = new HashMap<String, String>();
		        				param.put("type", typeComplexActual);
		        	            
		        				String salida= utilFuncion.formatString(esqueleto, param);
		        				System.out.println(salida);
		        				escrituraXsd(salida,false);
		        				
		        				complexTypeAbierto=true;
		        				listaComplex.poll();
		            		}
			            	
		            	}
		            	else{
		            		if(complexTypeAbierto){
		    					esqueleto= utilFuncion.getValor(PROPERTIE, ELEMENTO_COMPLEX_TYPE_FIN , EST);
		    					System.out.println(esqueleto);
		    					escrituraXsd(esqueleto,false);
		    					complexTypeAbierto=false;
		    					
		    					String[] dato = listaComplex.peek().split(";");
		    					nivelComplexActual = dato[0];
		    					typeComplexActual = dato[1];
		    				}
		            		esqueleto = utilFuncion.getValor(PROPERTIE, ELEMENTO_COMPLEX_TYPE_INI, EST);
	        				
	        				Map<String, String> param = new HashMap<String, String>();
	        				param.put("type", typeComplexActual);
	        	            
	        				String salida= utilFuncion.formatString(esqueleto, param);
	        				System.out.println(salida);
	        				escrituraXsd(salida,false);
	        				complexTypeAbierto=true;
	        				listaComplex.poll();
	        				
	    					esqueleto= utilFuncion.getValor(PROPERTIE, ELEMENTO_XSD_COMPLEX , EST);
	    					if(elementXsd.isEsComplextType()){
			            		paramsDetRea.put("type", "tns:"+elementXsd.getType());
			            	}
							salida= utilFuncion.formatString(esqueleto, paramsDetRea);
							System.out.println(salida);
							escrituraXsd(salida,false);
		            	}
		            }
					
					System.out.println("elementXsd.getNivel() "+elementXsd.getNivel()+" :: "+(elementXsd.getNivel().indexOf(SEPARADOR_NIVEL ) > -1));
				}
				//se termina la 1 capa, entonces pregunto si existe una segunda y si existe   crear complext
				//se indica que se creo una capa con true y luego del for j se confirma para cerrar el complex
				
				if(complexTypeAbierto){
					esqueleto= utilFuncion.getValor(PROPERTIE, ELEMENTO_COMPLEX_TYPE_FIN , EST);
					System.out.println(esqueleto);
					escrituraXsd(esqueleto,false);
					complexTypeAbierto=false;
				}
				
				if(capasIngresadas==i){
					System.out.println("Fin Creacion XSD, exito!!");
					if(complexTypeAbierto){
						esqueleto= utilFuncion.getValor(PROPERTIE, ELEMENTO_COMPLEX_TYPE_FIN , EST);
						System.out.println(esqueleto);
						escrituraXsd(esqueleto,false);
						complexTypeAbierto=false;
					}
				}
				else{
					System.out.println("aun quedan datos");
					esqueleto = utilFuncion.getValor(PROPERTIE, ELEMENTO_COMPLEX_TYPE_INI, EST);
					
					String[] dato = listaComplex.peek().split(";");
					nivelComplexActual = dato[0];
					typeComplexActual = dato[1];
					
					Map<String, String> paramsDetRea = new HashMap<String, String>();
		            paramsDetRea.put("type", typeComplexActual);
		            
					String salida= utilFuncion.formatString(esqueleto, paramsDetRea);
					System.out.println("salida:: "+salida);
					escrituraXsd(salida,false);
					
					complexTypeAbierto=true;
					listaComplex.poll(); 
				}
			}
		}
		catch (Exception e) {
			System.out.println("Exception [constructorXsd] ::"+e);
		}
		escrituraXsd("",true);
	}

	/**
	 * Escribe el resultado del programa en el archivo de salida
	 * @param contenido
	 */
	public static void escrituraXsd(String contenido, boolean cerrarArchivo){
		try {
			if(!cerrarArchivo){
				BufferedWriter bw = new BufferedWriter(new FileWriter("./archivosOut/"+XSD_SALIDA,true));
				bw.write(contenido);
				bw.write("\n");
				bw.close();
			}
			else{
				BufferedWriter bw = new BufferedWriter(new FileWriter("./archivosOut/"+XSD_SALIDA,true));
				bw.write("</xsd:schema>");
				bw.close();
			}
		}
		catch (Exception e) {
			System.out.println("Exception [escrituraXsd] ::"+e);
		}
	}

	public static List<List<datosXsdTO>> getLISTA_ESQUEMA() {
		return LISTA_ESQUEMA;
	}

	public static void setLISTA_ESQUEMA(List<List<datosXsdTO>> lISTA_ESQUEMA) {
		LISTA_ESQUEMA = lISTA_ESQUEMA;
	}
	
}